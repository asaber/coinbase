const express = require('express')
var app = express()
const hbs = require('hbs')
const coinapi = require('./coinapi.js');


app.set('view engine' , 'hbs');
app.use(express.static(__dirname + '/public'));
hbs.registerPartials(__dirname + '/views/partials');

app.get('/', (req , res)=>{
    console.log('hello from home');
    res.render('home.hbs', {
        pageTitle: ' home page' ,
        message : 'اسعار العملات الرقمية من كوين ماركت كاب',
        currentYear : new Date().getFullYear(),
        coin : coinapi.coins ,
        price : coinapi.price

    })
});

app.get('/bad', (req, res)=>{
    console.log('baaad');
 res.send({
     erroeMessage : 'can not fullfil'
 })
});

app.listen(3000, ()=>{
    console.log("app working on 3000")
});
